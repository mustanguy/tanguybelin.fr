$(document).ready(function(){
    $('.fade').delay(1000).fadeOut();

    var dict = {
        "Tanguy BELIN - Web Développeur": {
            fr: "Tanguy BELIN - Web Développeur",
            en: "Tanguy BELIN - Web Developer"
        },"Web Developpeur": {
            fr: "Web Developpeur",
            en: "Web Developer"
        },"A propos": {
            fr: "A propos",
            en: "About"
        },"Compétences et Projets": {
            fr: "Compétences et Projets",
            en: "Skills and Projects"
        },"A propos ...": {
            fr: "A propos ...",
            en: "About ..."
        },"Passionné depuis toujours par le développement, j'ai de nombreuses expériences en front-end et back-end.": {
            fr: "Passionné depuis toujours par le développement, j'ai de nombreuses expériences en front-end et back-end.",
            en: "With a degree in Web Development and subsequent employment as a result of my keen interest, I acquire many skills and experience in Front-End and Back-End"
        },"J'ai une bonne maîtrise de technologies tel que Symfony, Magento, Bootstrap, Jquery, Saas, ...": {
            fr: "J'ai une bonne maîtrise de technologies tel que Symfony, Magento, Bootstrap, Jquery, Saas, ...",
            en: "I work with an assortment of technologies, such as, Magento, Symfony, Bootstrap, Jquery, Lamp, Git, Jira..."
        },"Ces deux dernières années, j'ai eu la chance de travailler dans une grande entreprise nommé GFI Informatique en tant que développeur Back-end sur le pôle e-commerce Magento.": {
            fr: "Ces deux dernières années, j'ai eu la chance de travailler dans une grande entreprise nommé GFI Informatique en tant que développeur Back-end sur le pôle e-commerce Magento.",
            en: "In the last 2 years, I had the chance to work for GFI Informatique as a Back-End web developer on the framework Magento !"
        },"Je suis aussi freelance depuis trois ans, ce qui m'a permis de participer a de nombreux projets mêlant mes deux technologies préférées : Symfony et Magento !": {
            fr: "Je suis aussi freelance depuis trois ans, ce qui m'a permis de participer a de nombreux projets mêlant mes deux technologies préférées : Symfony et Magento !",
            en: "I also do freelance work which enables me to participate in different projects with my two favourite technologies : Symfony and Magento"
        },"N'hésitez pas a me contacter pour n'importe quelle demandes/propositions. Je me ferais une joie d'y répondre !": {
            fr: "N'hésitez pas a me contacter pour n'importe quelle demandes/propositions. Je me ferais une joie d'y répondre !",
            en: "Feel free to contact me for everything you want, it will be my pleasure to answer you !"
        },"Passe temps": {
            fr: "Passe temps",
            en: "Hobbies"
        },"Compétences": {
            fr: "Compétences",
            en: "Skills"
        },"Projets": {
            fr: "Projets",
            en: "Projects"
        }
    };

    if(navigator.language === "fr-FR") {
        $('html').translate({lang: "fr", t: dict});
    } else {
        $('html').translate({lang: "en", t: dict});
    }

    $('.fr').on('click', function (e) {
       e.preventDefault();
        $('html').translate({lang: "fr", t: dict});
    });

    $('.en').on('click', function (e) {
        e.preventDefault();
        $('html').translate({lang: "en", t: dict});
    });

    $('.fa-phone').on('mouseover', function () {
        $('.phone-number').fadeIn();
    });

    $('.fa-phone').on('mouseleave', function () {
        $('.phone-number').fadeOut();
    });
    created = 0;
    $('.side-right .intro-content').on('click', function (e) {
        if (created !== 1) {
            setTimeout(function () {
                $('.bar').each(function (a,b) {
                    createBar(this, $(this).attr('data-color1'), $(this).attr('data-color2'), $(this).attr('data-number'), $(this).attr('data-text'));
                });
            }, 300)

            created = 1;
        }
    });

    function createBar (container, color1, color2, number, text) {
        var bar = new ProgressBar.Circle(container, {
            color: color1,
            trailColor: '#fff',
            trailWidth: 2,
            duration: 1400,
            easing: 'bounce',
            strokeWidth: 6,
            text: {
              value: text
            },
            from: {color: color2, a:0},
            to: {color: color1, a:1},
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
            }
        });

        bar.animate(number)  // Number from 0.0 to 1.0
    }

});

